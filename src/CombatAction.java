
public interface CombatAction {
	public void attack();
	public void defend();
	public void flee();
}
