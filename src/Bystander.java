
public class Bystander implements CombatAction{
	public void attack() {
		System.out.println(".........");
	}
	public void defend() {
		System.out.println("The NPC attacks.");
	}
	
	public void flee() {
		System.out.println("The NPC run away screaming.");
	}
	
	public String message() {
		String npcMessage = "You dare challenge me, pleb?";
		return npcMessage;
	}
}
