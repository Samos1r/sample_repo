
public class CharacterTest {

	public static void main(String[] args) {
		Warrior warrior = new Warrior();
		Bystander npc = new Bystander();
		
		System.out.println(npc.message());
		npc.attack();
		warrior.defend();
		warrior.attack();
		npc.defend();
		warrior.attack();
		npc.flee();
		System.out.println("Warrior gains :"+warrior.gainExp()+" XP");
	}

}
