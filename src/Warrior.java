import java.security.SecureRandom;
public class Warrior implements CombatAction{
	
	SecureRandom secureRandom = new SecureRandom();
	
	public void attack() {
		System.out.println("The warrior swings his sword.");
	}
	
	public void defend() {
		System.out.println("The warrior covers himself with his shield.");
	}
	
	public void flee() {
		System.out.println("The warrior escapes the battle zone.");
	}
	
	public int gainExp() {
		return secureRandom.nextInt(500);
	}
}
